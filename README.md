Based on [Heroku Django Starter Template](https://github.com/heroku/heroku-django-template)

## Running Locally

Make sure you have Python, Pip and Virtualenv.  Also, install the [Heroku Toolbelt](https://toolbelt.heroku.com/) and [Postgres](https://devcenter.heroku.com/articles/heroku-postgresql#local-setup).

```sh
$ git clone https://github.com/tuangu/iot-server.git
$ cd iot-server

$ virtualenv venv
$ source venv/bin/activate

$ createdb iot

$ python manage.py migrate
$ python manage.py collectstatic

$ heroku local
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```sh
$ heroku create
$ git push heroku master

$ heroku run python manage.py migrate
$ heroku open
```

## Endpoints

| ENDPOINT | USAGE |
|----------|----------|
| GET /sensors | List sensors' data |
| GET /sensors?start={start_timestamp}?end={end_timestamp} | Filter data by time |
| GET /sensors/{id} | Get sensors' data by id |
| POST /sensors | Creates new data and return its details. Data is in json format. |
| PUT /sensors/{id} | Update sensors' data |
| DELETE /sensors/{id} | Delete sensors' data |
| GET /match | List match's data|
| POST /match | Get list of match data|
| GET/ match/{id} | Get match data by id |
| PUT/ match/{id} | Update match data |
| DELETE/ match/{id} | Delete match data |

## References
- [Getting Started with Python on Heroku](https://devcenter.heroku.com/articles/getting-started-with-python)
- [Heroku Django Starter Template](https://github.com/heroku/heroku-django-template)