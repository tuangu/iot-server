from django.conf.urls import url
from api import views

urlpatterns = [
    url(r'^sensors/?$', views.SensorDataList.as_view()),
    url(r'^sensors/(?P<pk>[0-9]+)/$', views.SensorDataDetail.as_view()),
    url(r'^match/?$', views.MatchDataList.as_view()),
    url(r'^match/(?P<pk>[0-9]+)/$', views.MatchDataDetail.as_view()),
]