from django.db import models

# from django_unixdatetimefield import UnixDateTimeField

class SensorData(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    timestamp = models.BigIntegerField(default=0)
    accel_x = models.FloatField()
    accel_y = models.FloatField()
    accel_z = models.FloatField()

    class Meta:
        ordering = ('created', )

class MatchData(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    kick_count = models.PositiveIntegerField()
    max_speed = models.FloatField()

    class Meta:
        ordering = ('created', )

