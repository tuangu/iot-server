from datetime import datetime

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination

from api.models import SensorData
from api.models import MatchData

from api.serializers import SensorDataSerializer
from api.serializers import MatchDataSerializer

class SensorDataList(APIView):
    """
    List all sensors' data, or create a new instance
    """

    def get(self, request, format=None):
        now = timezone.now().timestamp()
        start_at = float(request.GET.get("start", default=0))
        end_at = float(request.GET.get("end", default=now))

        sensor_data = SensorData.objects.filter(created__gte=timezone.make_aware(datetime.fromtimestamp(start_at)),
                                        created__lte=timezone.make_aware(datetime.fromtimestamp(end_at)))

        paginator = PageNumberPagination()
        paginator.page_size = 100

        #page = self.paginate_queryset(self, sensor_data)
        page = paginator.paginate_queryset(sensor_data, request)
        if sensor_data is not None:
            serializer = SensorDataSerializer(page, many=True)
            return paginator.get_paginated_response(serializer.data)

        serializer = self.get_serializer(sensor_data, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        datas = JSONParser().parse(request)
        serializers = []

        for data in datas['data']: 
            serializer = SensorDataSerializer(data=data)
            
            serializers.append(serializer)
            if not(serializer.is_valid()):
                return JsonResponse(serializer.errors, status=400)

        for serializer in serializers:
            serializer.save()
                
        return Response(datas, status=201)

class SensorDataDetail(APIView):
    """
    Retrieve, update or delete a sensor_data instance.
    """
    def get_object(self, pk):
        try:
            return SensorData.objects.get(pk=pk)
        except SensorData.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        sensor_data = self.get_object(pk)
        serializer = SensorDataSerializer(sensor_data)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        sensor_data = self.get_object(pk)
        serializer = SensorDataSerializer(sensor_data, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def delete(self, request, pk, format=None):
        sensor_data = self.get_object(pk)
        sensor_data.delete()
        return Response(status=204)

class MatchDataList(APIView):
    """
    List all match data or create a new instance
    """

    def get(self, request, format=None):
        match_data = MatchData.objects.all()

        paginator = PageNumberPagination()
        paginator.page_size = 100

        page = paginator.paginate_queryset(match_data, request)
        if match_data is not None:
            serializer = MatchDataSerializer(page, many=True)
            return paginator.get_paginated_response(serializer.data)

        serializer = self.get_serializer(match_data, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        data = JSONParser().parse(request)

        serializer = MatchDataSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(data, status=201)

        return JsonResponse(serializer.errors, status=400)

class MatchDataDetail(APIView):
    """
    Retrieve, update or delete a match_data instance.
    """
    def get_object(self, pk):
        try:
            return MatchData.objects.get(pk=pk)
        except MatchData.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        match_data = self.get_object(pk)
        serializer = MatchDataSerializer(match_data)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        match_data = self.get_object(pk)
        serializer = MatchDataSerializer(match_data, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def delete(self, request, pk, format=None):
        match_data = self.get_object(pk)
        match_data.delete()
        return Response(status=204)