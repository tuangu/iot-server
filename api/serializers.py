from rest_framework import serializers

from api.models import SensorData
from api.models import MatchData

class SensorDataSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    created = serializers.DateTimeField(read_only=True)
    timestamp = serializers.IntegerField()
    accel_x = serializers.FloatField()
    accel_y = serializers.FloatField()
    accel_z = serializers.FloatField()

    def create(self, validated_data):
        """
        Create and return a new `SensorData` instance, given the validated data.
        """
        return SensorData.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `SensorData` instance, given the validated data.
        """
        instance.created = validated_data.get('created', instance.created)
        instance.timestamp = validated_data.get('timestamp', instance.timestamp)
        instance.accel_x = validated_data.get('accel_x', instance.accel_x)
        instance.accel_y = validated_data.get('accel_y', instance.accel_y)
        instance.accel_z = validated_data.get('accel_z', instance.accel_z)
        instance.save()
        return instance

class MatchDataSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    created = serializers.DateTimeField(read_only=True)
    kick_count = serializers.IntegerField()
    max_speed = serializers.FloatField()

    def create(self, validated_data):
        """
        Create and return a new `MatchData` instance, given the validated data.
        """
        return MatchData.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `MatchData` instance, given the validated data.
        """
        instance.created = validated_data.get('created', instance.created)
        instance.kick_count = validated_data.get('kick_count', instance.kick_count)
        instance.max_speed = validated_data.get('max_speed', instance.max_speed)
